package main

import (
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	h1 := func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Hello world from Golang\n")
	}

	h2 := func(w http.ResponseWriter, r *http.Request) {
		client := &http.Client{}
		url := "https://jsonplaceholder.typicode.com/posts"
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			panic(err)
		}

		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}

		defer resp.Body.Close()

		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		io.WriteString(w, string(bodyBytes))
	}

	http.HandleFunc("/", h1)
	http.HandleFunc("/get", h2)

	log.Println("Running server on port 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
